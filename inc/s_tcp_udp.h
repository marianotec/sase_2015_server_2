/**
 * @file   c_tcp_udp.h
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   31/7/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#pragma once

/* **************************************************************** */
/* 								INCLUDEs							*/

#include <sys/types.h>

#include <sys/socket.h>

#include <netinet/in.h>

#include <arpa/inet.h>

#include <stdio.h>

#include <unistd.h>

#include <errno.h>

#include <string.h>

#include <stdlib.h>

#include <netdb.h>

/* **************************************************************** */
/* 								DEFINEs								*/

// CONFIGURATION DEFINEs

#define BACKLOG_TCP 20	// Tamaño de la cola de conexiones recibidas

// GENERIC DEFINEs

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

// OTHER DEFINEs

/* **************************************************************** */
/* 								IFDEFs								*/

//	Add ifdef's here

/* **************************************************************** */
/* 								MACROs								*/

//	Add macro's here

/* **************************************************************** */
/* 						     EXTERNAL GLOBALs						*/

//	Add global variable's here

/* **************************************************************** */
/* 						     	PROTOTYPEs							*/

int tcp_connect (char * server_ip, int server_port, struct sockaddr_in *their_addr);				// Funcion para conectarse con el servidor

int tcp_openConnection ( int server_port, struct sockaddr_in * my_addr);							// Función que crea la conexión tcp (Servidor)

int tcp_connectionClient (int sockfd, struct sockaddr_in * their_addr);								// Función que acepta una conexión entrante (Servidor)

ssize_t tcp_sendData (int fd,void * data,int bytes);

ssize_t tcp_recvData(int fd,void * data, int bytes);


int udp_connectionClient (char * server_ip, int server_port, struct sockaddr_in * their_addr);		//Funcion para conectarse a un servidor udp

int udp_connectionServer ( int server_port, struct sockaddr_in * my_addr);							//Funcion que crea una conexion udp (Servidor)

ssize_t udp_sendData(int fd, void * data, int bytes,struct sockaddr_in  dest_addr );

ssize_t udp_recvData(int fd, void * data, int bytes, struct sockaddr_in * src_addr);
