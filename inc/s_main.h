/**
 * @file   s_main.h
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef INC_S_MAIN_H_
#define INC_S_MAIN_H_

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	<stdio.h>

#include	<stdlib.h>

#include	<unistd.h>

#include	<strings.h>

#include	<sys/select.h>

#include	<sys/types.h>

#include	<sys/socket.h>

#include	<sys/wait.h>

#include	<netinet/in.h>

#include	<arpa/inet.h>

#include	<errno.h>

#include	<netdb.h>

#include	"s_tcp_udp.h"

#include	"s_auxFunctions.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// CONFIGURATION DEFINEs
#define		TCP_SERVER_PORT			3490

#define		UDP_SERVER_PORT			(TCP_SERVER_PORT+1)

#define		MAX_CONNECTIONS			5

#define		USEC_SELECT_TIMEOUT		10

#define		SEC_SELECT_TIMEOUT		0

#define		COMMAND_BUFFER_SIZE		50

#define		MAX_MESSAGE				20

// GENERIC DEFINEs
#define		TRUE					1

#define		FALSE					0

// FUNCTIONAL DEFINEs (DO NOT CHANGE)
#define		KEYBOARD_FD				0

#define 	PIPE_READ				0

#define 	PIPE_WRITE				1

#define 	PIPE_WR					2

#define		NO_FLAG					0

// OTHER DEFINEs
#define		TIMEOUT_VAL				0

#define		SELECT_ERROR_CODE		-2

/* **************************************************************** */
/* 								IFDEFs								*/

//	Add ifdef's here

/* **************************************************************** */
/* 								MACROs								*/

#define		IS_ERROR(x)					( (0 > x)  ? 1:0 )

#define		IS_TIMEOUT(x)				( (0 == x) ? 1:0 )

#define 	KEEP_RUNNING(x)				( (0 == x) ? 1:0 )

#define 	IS_CHILD_PROCCESS(x)		( (0 == x) ? 1:0 )

/* **************************************************************** */
/* 						     EXTERNAL GLOBALs						*/

//	Add global variable's here

/* **************************************************************** */
/* 						     	PROTOTYPEs							*/

void	father_serverProcess	(int rPipeFD, int wPipeFD, int childPid, int tcpServerFD, int udpServerFD, struct sockaddr_in *tcpServerAddress, struct sockaddr_in *udpServerAddress);

void	child_serverProcess		(int rPipeFD, int wPipeFD, int tcpClientFD, struct sockaddr_in * tcpClientAddress);

#endif /* INC_S_MAIN_H_ */
