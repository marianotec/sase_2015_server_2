/**
 * @file   s_auxFunctions.h
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef INC_S_AUXFUNCTIONS_H_
#define INC_S_AUXFUNCTIONS_H_

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	<stdio.h>

#include 	<unistd.h>

#include 	<string.h>

#include	<signal.h>

#include 	<sys/types.h>

#include 	<sys/wait.h>


/* **************************************************************** */
/* 								DEFINEs								*/

// CONFIGURATION DEFINEs

// GENERIC DEFINEs

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

// OTHER DEFINEs

#define 	WRITE_ERROR_CODE		-1

/* **************************************************************** */
/* 								IFDEFs								*/

//	Add ifdef's here

/* **************************************************************** */
/* 								MACROs								*/

//	Add macro's here

/* **************************************************************** */
/* 						     EXTERNAL GLOBALs						*/

//	Add global variable's here

/* **************************************************************** */
/* 						     	PROTOTYPEs							*/

int		writeTo (int wFD, const char * message);

void 	ctrlC_signalHandler (int useless); //SIGINT

void 	child_signalHandler (int useless); //SIGCHLD

void	exitFunction (int *exit, int wFD, const char *message);


#endif /* INC_S_AUXFUNCTIONS_H_ */
