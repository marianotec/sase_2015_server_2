/**
 * DOXYGEN COMMENTS
 *
 * @file   s_child.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	"s_main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

#define		write2Client(x)		writeTo(tcpClientFD, (const char *) x)

#define		write2Father(x)		writeTo(wPipeFD, (const char *) x)

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

void child_serverProcess (int rPipeFD, int wPipeFD, int tcpClientFD, struct sockaddr_in * tcpClientAddress)
{
	/** ************    VARIABLE's DECLARATION    ************ **/

	int fixedMaxFD = -1;

	int exitVar = FALSE;							// Variable que indica si se desea salir del loop principal o no

	fd_set fdsMaster;							// Variable que utiliza el handler del Select

	fd_set fdsAuxiliar;							// Variable que utiliza el handler del Select

	char messageBuffer[MAX_MESSAGE];

	/** ****************************************************** **/

	/** ************    Seteos de la función Select    ************ **/

	FD_ZERO( &fdsMaster );

	FD_SET( rPipeFD, &fdsMaster ); 		//Seteo el pipe de lectura
	FD_SET( tcpClientFD, &fdsMaster ); 	//Seteo el socket TCP

	fixedMaxFD = ( ( tcpClientFD > rPipeFD ) ? tcpClientFD:rPipeFD ) + 1;

	/** ****************************************************** **/

	/**		MAIN LOOP		**/
	while(FALSE == exitVar)
	{
		bzero(messageBuffer,MAX_MESSAGE);

		fdsAuxiliar = fdsMaster; //Copio en Auxiliar el Master, dado que el mismo se "reinicia" en cada loop

		select( fixedMaxFD , &fdsAuxiliar , NULL , NULL , NULL );

		/**
		 * SELECT STATEMENT: "Interrupción" por PIPE de lectura (Proceso PADRE)
		 **/
		if( FD_ISSET( rPipeFD , &fdsAuxiliar ) == TRUE )
		{
			read(rPipeFD, messageBuffer, MAX_MESSAGE);

			if(0 == strcmp(messageBuffer,"exit"))	//Si se recibió comando de escape (eviado por el Padre)
			{
				printf("\nDebug(Proceso Hijo): Se recibió el comando de escape por el PADRE\n");

				//Informo al cliente que salgo (dado que el proceso padre me lo pide)
				exitFunction(&exitVar,tcpClientFD,"exit");
			}
		}

		/**
		 * SELECT STATEMENT: "Interrupción" por socket TCP (Cliente)
		 **/
		else if( FD_ISSET( tcpClientFD , &fdsAuxiliar ) == TRUE )
		{
			read(tcpClientFD, messageBuffer, MAX_MESSAGE);

			if(0 == strcmp(messageBuffer,"exit"))	//Si se recibió comando de escape (eviado por el Cliente)
			{
				printf("\nDebug(Proceso Hijo): Se recibió el comando de escape por el CLIENTE\n");

				//Informo al cliente que salgo (dado que el proceso padre me lo pide)
				exitFunction(&exitVar,wPipeFD,"exit");
			}
		}

		/**   END OF SELECT LOOP   **/

	}

	printf("\nDebug(Proceso Hijo): Cerrando el Child\n");

	close(rPipeFD);

	close(wPipeFD);

	close(tcpClientFD);

	exit(0);
}
