/**
 * DOXYGEN COMMENTS
 *
 * @file   s_main.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	"s_main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

#define		CLEAR_SCREEN		system("clear")

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

int main(void)
{
	/** ************    VARIABLE's DECLARATION    ************ **/

	int ServerToClientPipeFD[PIPE_WR];			// Vector que contiene los file descriptor de R/W de un pipe (Server to Client)

	int ClientToServerPipeFD[PIPE_WR];			// Vector que contiene los file descriptor de R/W de un pipe (Client to Server)

	int childPid = -1; 							// Variable donde se almacena el PID del proceso "hijo"

	int tcpServerFD = -1;						// Variable donde se almacena el file descriptor del socket TCP del servidor

	int udpServerFD = -1;						// Variable donde se almacena el file descriptor del socket UDP del servidor

	int tcpClientFD = -1;						// Variable donde se almacena el file descriptor del socket TCP asignado al cliente

	struct sockaddr_in tcpServerAddress;		// Estructura que contiene información útil sobre el socket TCP del servidor

	struct sockaddr_in tcpClientAddress;		// Estructura que contiene información útil sobre el socket TCP del cliente

	struct sockaddr_in udpServerAddress;		// Estructura que contiene información útil sobre el socket UDP del servidor

	/** ****************************************************** **/

	signal(SIGINT,ctrlC_signalHandler);

	CLEAR_SCREEN;

	printf("\nDebug(Servidor): Abriendo pipes y sockets...\n");

	//Abro los pipes para comunicar al proceso padre e hijo
	if( IS_ERROR( pipe(ServerToClientPipeFD) ) )
	{
		printf("\nDebug(Servidor): Error PIPE\n");

		return -1;
	}

	if( IS_ERROR( pipe(ClientToServerPipeFD) ) )
	{
		printf("\nDebug(Servidor): Error PIPE\n");

		return -1;
	}

	//Abro el socket UDPclear
	udpServerFD = udp_connectionServer( UDP_SERVER_PORT , &udpServerAddress );

	if(	IS_ERROR(udpServerFD)	)
	{
		printf("\nDebug(Servidor): Error UDP\n");

		return -1;
	}

	//Abro el socket TCP para recibir clientes
	tcpServerFD = tcp_openConnection( TCP_SERVER_PORT , &tcpServerAddress );

	if(	IS_ERROR(tcpServerFD)	)
	{
		printf("\nDebug(Servidor): Error TCP\n");

		return -1;
	}

	printf("\nDebug(Servidor): Los pipes y sockets se abrieron correctamente.\n");

	printf("\nDebug(Servidor): Haciendo fork...\n");

	//Creo un proceso hijo
	childPid = fork();

	if(IS_ERROR(childPid))
	{
		printf("\nDebug(Servidor): Error Fork\n");

		return -1;
	}

	printf("\nDebug(Servidor): Fork se realizo correctamente.\n");

	//Si no hubo errores de socket, de fork ni de pipe, se prosigue con la ejecución

	if(IS_CHILD_PROCCESS(childPid))
	{
		printf("\nDebug(Proceso Hijo): Hola! soy el hijo.\n");

		//Cierro los pipes innecesarios (Solo se cierran en el proceso hijo)
		close(ServerToClientPipeFD[PIPE_WRITE]);
		close(ClientToServerPipeFD[PIPE_READ]);

		printf("\nDebug(Proceso Hijo): Esperando solicitud de Conexión...\n");

		//Abro el socket TCP para atender a un cliente
		tcpClientFD = tcp_connectionClient( tcpServerFD , &tcpClientAddress );

		if( IS_ERROR(tcpClientFD) )
		{
			printf("\nDebug(Proceso Hijo): Error TCP\n");

			exit (-1);
		}

		printf("\nDebug(Proceso Hijo): Conexión recibida correctamente\n");

		//Accedo a la función principal del proceso Hijo
		child_serverProcess(ServerToClientPipeFD[PIPE_READ],ClientToServerPipeFD[PIPE_WRITE],tcpClientFD,&tcpClientAddress);

		return 0;
	}

	printf("\nDebug(Proceso Padre): Hola! soy el padre.\n");

	//Cierro los pipes innecesarios (Solo se cierran en el proceso padre)
	close(ServerToClientPipeFD[PIPE_READ]);
	close(ClientToServerPipeFD[PIPE_WRITE]);

	//Accedo a la función principal del proceso Padre
	father_serverProcess(ClientToServerPipeFD[PIPE_READ],ServerToClientPipeFD[PIPE_WRITE],childPid,tcpServerFD,udpServerFD,&tcpServerAddress,&udpServerAddress);

	printf("\nDebug(Proceso Padre): Se cierra el Servidor.\n");

	return 0;
}
