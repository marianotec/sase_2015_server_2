/**
 * DOXYGEN COMMENTS
 *
 * @file   s_father.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   3/8/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	"s_main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

#define		ENDLINE2NULL		for(index=0;messageBuffer[index]!='\n'&&messageBuffer[index]!='\0';index++);messageBuffer[index]='\0';index=0

#define		write2Child(x)		writeTo(wPipeFD, (const char *) x)

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

void father_serverProcess(int rPipeFD, int wPipeFD, int childPid, int tcpServerFD, int udpServerFD, struct sockaddr_in *tcpServerAddress, struct sockaddr_in *udpServerAddress)
{
	/** ************    VARIABLE's DECLARATION    ************ **/

	int index = 0;								// Variable utilizada para recorrer vectores y como auxiliar en lazos

	int count = 0;

	int exit = FALSE;							// Variable que indica si se desea salir del loop principal o no

	int selectValue = -1;						// Variable que toma el valor que retorna la función Select

	fd_set fdsMaster;							// Variable que utiliza el handler del Select

	fd_set fdsAuxiliar;							// Variable que utiliza el handler del Select

	struct timeval timeout;						// Estructura para setear el timeout del Select

	char messageBuffer[MAX_MESSAGE];			// Vector donde se guardan los mensajes recibidos

	struct sockaddr_in	udpClientAddress;

	/** ****************************************************** **/

	signal(SIGCLD,child_signalHandler);

	count = sizeof(udpClientAddress);

	recvfrom(udpServerFD,messageBuffer,MAX_MESSAGE,0,(struct sockaddr *)&udpClientAddress,(unsigned int *)&count);

	printf("\nDebug(Proceso Padre): Se recibió por UDP: \"%s\"\n",messageBuffer);

	/** ************    Seteos de la función Select    ************ **/

	FD_ZERO( &fdsMaster );

	//File Descriptos a monitorear
	FD_SET( KEYBOARD_FD, &fdsMaster );		//Seteo el teclado
	FD_SET( rPipeFD, &fdsMaster );			//Seteo el pipe de lectura

	//Seteos timeout
	timeout.tv_sec = SEC_SELECT_TIMEOUT;
	timeout.tv_usec = USEC_SELECT_TIMEOUT;

	/** ****************************************************** **/

	/**		MAIN LOOP		**/
	while(FALSE == exit)
	{
		bzero(messageBuffer,MAX_MESSAGE);		//Limpio el buffer de mensajes

		fdsAuxiliar = fdsMaster;		//Copio en Auxiliar el Master, dado que el mismo se "reinicia" en cada loop

		selectValue = select( rPipeFD+1 , &fdsAuxiliar , NULL , NULL , &timeout );

		/**
		 * SELECT STATEMENT: "Interrupción" por teclado
		 **/
		if( FD_ISSET( KEYBOARD_FD , &fdsAuxiliar ) == TRUE )
		{
			fgets(messageBuffer, MAX_MESSAGE, stdin);		//Capturo el comando ingresado por teclado

			ENDLINE2NULL;

			//Si las strings coinciden
			if(0 == strcmp(messageBuffer,"exit"))
			{
				printf("\nDebug(Proceso Padre): Se ingresó el comando de escape\n");

				exitFunction(&exit,wPipeFD,"exit");
			}
		}

		/**
		 * SELECT STATEMENT: "Interrupción" por pipe de lectura
		 **/
		else if( FD_ISSET( rPipeFD , &fdsAuxiliar ) == TRUE )
		{
			read(rPipeFD, messageBuffer, MAX_MESSAGE);

			if(0 == strcmp("exit",messageBuffer))	exit = TRUE;
		}

		/**
		 * SELECT STATEMENT: "Interrupción" por timeout
		 **/
		else if( IS_TIMEOUT( selectValue ) )
		{
			count++;

			sprintf(messageBuffer,"%d",count);

			sendto(udpServerFD,messageBuffer,strlen(messageBuffer),0,(struct sockaddr *)&udpClientAddress,sizeof(udpClientAddress));
		}
	}

	printf("\nDebug(Proceso Padre): Cerrando la aplicación...\n");

	//wait(NULL);
	sleep(1);

	close(rPipeFD);

	close(wPipeFD);

	close(tcpServerFD);

	close(udpServerFD);

	return;
}
